import java.util.ArrayList;

public class AdjacencyListGraph {
	
	public AdjacencyListGraph(){
		nodes = new ArrayList<ArrayList<Node>>();
		
		for(char c='A';c<='Z';c++){
			Node newNode = new Node(c,0,0);
			this.addNode(newNode);
		}
	}
	
	public void addNode(Node newNode){
		ArrayList<Node> newList = new ArrayList<Node>();
		newList.add(newNode);
		nodes.add(newList);
		
	}
	
	public Node getNode(char from,char to){
		for(ArrayList<Node> arrayNode : nodes){
			if(arrayNode.get(0).getName() == from){
				for(Node n : arrayNode){
					int index = arrayNode.indexOf(n);
					if(n.getName() == to && index != 0){
						return n;
					}
				}
			}
		}
		return null;
	}
	
	public void updateNode(Node from, Node to){
		for(ArrayList<Node> arrayNode : nodes){
			if(arrayNode.get(0).getName() == from.getName()){
				for(Node n : arrayNode){					
					int index = arrayNode.indexOf(n);
					if(n.getName() == to.getName() && index != 0){
						arrayNode.set(index, to);
					}
				}
			}
			
		}
	}
  
	public void addEdge(char from, char to, int delay, int capacity){
		int index = from - 'A';
		ArrayList<Node> a = nodes.get(index);
		Node newEdge = new Node(to,delay,capacity);
		a.add(newEdge);
	}

	public boolean hasEdge(char from, char to){
		int index = from - 'A';
		ArrayList<Node> a = nodes.get(index);
		
		for(Node n : a){
			if(n.getName() == to){
				return true;
			}
		}
		return false;
	}

	public ArrayList<Node> getConnectedNodes(char node){
		int index = node - 'A';
		ArrayList<Node> a = new ArrayList<Node>(nodes.get(index)); //clone it
		a.remove(0);
		return a;
	}
  
	private ArrayList<ArrayList<Node>> nodes;
}
