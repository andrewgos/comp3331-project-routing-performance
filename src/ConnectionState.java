import java.util.*;

public class ConnectionState {
	public ConnectionState(char from, char to, ArrayList<Character> p, double d, boolean stat, double timeToLive){
		source = from;
		destination = to;
		path = p;
		time = d;
		status = stat;
		duration = timeToLive;
	}
	
	public ArrayList<Character> getPath(){
		return path;
	}
	
	public double getTime(){
		return time;
	}
	
	public char getSource(){
		return source;
	}
	
	public char getDestination(){
		return destination;
	}
	
	public boolean getStatus(){
		return status;
	}
	
	public double getDuration(){
		return duration;
	}
	private ArrayList<Character> path;
	private double time;
	private char source;
	private char destination;
	private boolean status; //false for stop, true for start
	private double duration;
	
}
