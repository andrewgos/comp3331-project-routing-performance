import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;


public class DijkstraAlgorithm {
	
	private static int[] weight;
	private static char[] parent;
	private static boolean[] blocked;

	public static ArrayList<Character> findShortestPath(AdjacencyListGraph graph, char from, char to){
		weight = new int[26];
		parent = new char[26];
		blocked = new boolean[26];

		//need a source and a destination variable to be passed in
		ArrayList<Character> shortestPath = new ArrayList<Character>();

		//make priority queue
		EdgeComparator comparator = new EdgeComparator();
		PriorityQueue<Edge> pq = new PriorityQueue<Edge>(11, comparator);
		char currPos = from;
		
		//initialise array
		for(int i=0; i<26; i++) {
			weight[i] = Integer.MAX_VALUE;
			blocked[i] = false;

		}
		
		//source weight is 0
		weight[currPos-'A'] = 0;
		parent[currPos-'A'] = currPos;

		Edge s = new Edge(currPos, currPos, 0);
		pq.add(s);
		
		//get all the nodes that the source connects to
		while (!pq.isEmpty()) {
			Edge currEdge = pq.poll();
			
			//don't reprocess a visited node
			List<Node> connectedFrom = graph.getConnectedNodes(currEdge.getDestination());

			//loop through each node that are connected
			for(Node n : connectedFrom) {
				
				//add to priority queue, the node and total weight
				if (n.getDelay()+ weight[currEdge.getDestination()-'A'] < weight[n.getName()-'A']) {
					//if current total weight is less than existing total weight, replace it
					// and also replace the parent, put the edge in queue
					weight[n.getName()-'A']= n.getDelay() + weight[currEdge.getDestination()-'A'];
					parent[n.getName()-'A']= currEdge.getDestination();
					if(n.getLoad() == 1){
						blocked[n.getName()-'A'] = true;
					}
					pq.add(new Edge(currEdge.getDestination(), n.getName(), weight[n.getName()-'A']));
				}
			}			
		}
		
		//backtracks from the parent
		currPos = to;
		while (currPos != from) {
		    shortestPath.add(currPos);
		    if(currPos == 0 || blocked[currPos-'A'] == true){
				shortestPath.clear();
				return shortestPath;
			}
		    currPos = parent[currPos-'A'];
		}
	    shortestPath.add(currPos);
	    Collections.reverse(shortestPath);
		return shortestPath;
	}
	
	private static class EdgeComparator implements Comparator<Edge> {
		public int compare(Edge e1, Edge e2) {
			if ((e1.getWeight() > e2.getWeight()) ) return 1;
			if ((e2.getWeight() > e1.getWeight()) ) return -1;
			return 0;
		}
	}
}
