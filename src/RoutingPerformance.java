import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class RoutingPerformance {

	public static void main(String[] args) {
		try
		{
			String networkScheme = args[0];
			String routingScheme = args[1];
			double packetRate = Double.parseDouble(args[4]);
			
			double totalCircuitRequest = 0;
			double totalPackets = 0;
			double totalSuccessPackets = 0;
			double totalBlockedPackets = 0;
			double totalHops = 0;
			double totalDelay = 0;
			
			graph = new AdjacencyListGraph();
			path = new ArrayList<Character>();

			//LOAD THE GRAPH FROM TOPOLOGY FILE
			sc = new Scanner(new FileReader(args[2]));// args[2] is for TOPOLOGY file			
			while(sc.hasNextLine()){ // taking every inputs and convert it into ArrayList of Line
				String lineOfInput = sc.nextLine();
				String[] input = lineOfInput.split(" ");				

				char from = input[0].charAt(0);
				char to = input[1].charAt(0);
				int delay= Integer.parseInt(input[2]);
				int capacity = Integer.parseInt(input[3]);

				graph.addEdge(from, to, delay, capacity);
				graph.addEdge(to, from, delay, capacity);
			}
			/*
			 #####    ###   ######   #####  #     #   ###   #######
			 #     #    #    #     # #     # #     #    #       #
			 #          #    #     # #       #     #    #       #
			 #          #    ######  #       #     #    #       #
			 #          #    #   #   #       #     #    #       #
			 #     #    #    #    #  #     # #     #    #       #
			  #####    ###   #     #  #####   #####    ###      #
			 */
			if(networkScheme.equals("CIRCUIT")){
				
				//LOADING THE WORKLOAD FILE
				ConnectionComparator comparator = new ConnectionComparator();
				PriorityQueue<ConnectionState> pq = new PriorityQueue<ConnectionState>(11, comparator);
				double currTime = 0;

				sc2 = new Scanner(new FileReader(args[3])); // args[3] is for WORKLOAD file 
				while(sc2.hasNextLine()){ // taking every inputs and convert it into ArrayList of Line
					String lineOfInput = sc2.nextLine();
					String[] input = lineOfInput.split(" ");

					currTime = Double.parseDouble(input[0]);
					char from = input[1].charAt(0);
					char to = input[2].charAt(0);
					double timeDuration = Double.parseDouble(input[3]);

					while(pq.peek() != null && pq.peek().getTime() < currTime){ //check if need to close any connection
						ConnectionState currState = pq.poll();
						ArrayList<Character> currPath = currState.getPath();
						updateGraph(currPath,-1);
					}

					if(routingScheme.equals("SHP")){
						path = BFS.findShortestPath(graph, from, to);
					}
					else if(routingScheme.equals("SDP")){
						path = DijkstraAlgorithm.findShortestPath(graph, from, to);
					}
					else if(routingScheme.equals("LLP")){
						path = LeastLoadedPath.findShortestPath(graph, from, to);
					}
					
					if(!path.isEmpty()){ //if the packet is not dropped
						updateGraph(path, 1);
						ConnectionState newState = new ConnectionState(from, to, path, currTime+timeDuration, false, 0);
						pq.add(newState);
						
						totalSuccessPackets += Math.ceil(timeDuration*packetRate);
						totalHops += path.size()-1;
						totalDelay += getPathDelay(path);
					}
					else{
						totalBlockedPackets += Math.ceil(timeDuration*packetRate);
					}
					totalCircuitRequest++;
					totalPackets += Math.ceil(timeDuration*packetRate);
				}
				
				
			}

			/*
			 #####     ##     ####   #    #  ######   #####
			 #    #   #  #   #    #  #   #   #          #
			 #    #  #    #  #       ####    #####      #
			 #####   ######  #       #  #    #          #
			 #       #    #  #    #  #   #   #          #
			 #       #    #   ####   #    #  ######     #
			 */
			else if(networkScheme.equals("PACKET")){
				//LOADING THE WORKLOAD FILE
				ConnectionComparator comparator = new ConnectionComparator();
				PriorityQueue<ConnectionState> pq = new PriorityQueue<ConnectionState>(11, comparator);
				

				sc2 = new Scanner(new FileReader(args[3])); // args[3] is for WORKLOAD file 
				while(sc2.hasNextLine()){ // taking every inputs and convert it into ArrayList of Line
					String lineOfInput = sc2.nextLine();
					String[] input = lineOfInput.split(" ");

					double startTime = Double.parseDouble(input[0]);
					double currTime = startTime;
					char from = input[1].charAt(0);
					char to = input[2].charAt(0);
					double timeDuration = Double.parseDouble(input[3]);
					double packetsToBeSent = Math.ceil(timeDuration)*packetRate;
					
					
					for (int i=0; i < (int)packetsToBeSent; i++) {
						ConnectionState newState;
						
						if(packetsToBeSent == i-1){ //last packet of the connection / line
							newState = new ConnectionState(from, to, null, startTime+(i/packetRate), true, startTime+timeDuration - startTime+(i/packetRate));
		
						}
						else{
							newState = new ConnectionState(from, to, null, startTime+(i/packetRate), true, 1/packetRate-0.000001);

						}
						pq.add(newState);
						totalPackets++;
					}
					
					//OPEN AND CLOSE ANY CONNECTION THAT PASSED THE TIME
					while(pq.peek() != null && pq.peek().getTime() < currTime){
						ConnectionState currState = pq.poll();
						if (currState.getStatus() == true) {
							if(routingScheme.equals("SHP")){
								path = BFS.findShortestPath(graph, currState.getSource(), currState.getDestination());
							}
							else if(routingScheme.equals("SDP")){
								path = DijkstraAlgorithm.findShortestPath(graph, currState.getSource(), currState.getDestination());
							}
							else if(routingScheme.equals("LLP")){
								path = LeastLoadedPath.findShortestPath(graph, currState.getSource(), currState.getDestination());
							}
							
							if(!path.isEmpty()){ //if the packet is not dropped
								updateGraph(path, 1);
								ConnectionState newState;								
								newState = new ConnectionState('a', 'a', path, currState.getTime()+currState.getDuration(), false, 0);
								pq.add(newState);
								
								totalSuccessPackets++;
								totalHops += path.size()-1;
								totalDelay += getPathDelay(path);
							}
							else{
								totalBlockedPackets++;
							}
							
						}
						else {

							ArrayList<Character> currPath = currState.getPath();
						    updateGraph(currPath,-1);
						}
					}
				}
				
				//OPEN AND CLOSE ANY CONNECTION LEFT
				while(pq.peek() != null){
					ConnectionState currState = pq.poll();
					if (currState.getStatus() == true) {
						if(routingScheme.equals("SHP")){
							path = BFS.findShortestPath(graph, currState.getSource(), currState.getDestination());
						}
						else if(routingScheme.equals("SDP")){
							path = DijkstraAlgorithm.findShortestPath(graph, currState.getSource(), currState.getDestination());
						}
						else if(routingScheme.equals("LLP")){
							path = LeastLoadedPath.findShortestPath(graph, currState.getSource(), currState.getDestination());
						}
						
						if(!path.isEmpty()){ //if the packet is not dropped
							updateGraph(path, 1);
							ConnectionState newState;								
							newState = new ConnectionState('a', 'a', path, currState.getTime()+currState.getDuration(), false, 0);
							pq.add(newState);
							
							totalSuccessPackets++;
							totalHops += path.size()-1;
							totalDelay += getPathDelay(path);
						}
						else{
							totalBlockedPackets++;
						}
						
					}
					else {

						ArrayList<Character> currPath = currState.getPath();
					    updateGraph(currPath,-1);
					}
				}
				totalCircuitRequest = totalPackets;
			}
			
			DecimalFormat twoDP = new DecimalFormat("#.##");
			DecimalFormat zeroDP = new DecimalFormat("#");

			System.out.println("total number of virtual circuit requests: " + zeroDP.format(totalCircuitRequest) +"\n");
			System.out.println("total number of packets: " + zeroDP.format(totalPackets)+"\n");
			System.out.println("number of successfully routed packets: " + zeroDP.format(totalSuccessPackets)+"\n");
			System.out.println("percentage of successfully routed packets: " + twoDP.format((totalSuccessPackets/totalPackets)*100)+"\n");
			System.out.println("number of blocked packets: " + zeroDP.format(totalBlockedPackets)+"\n");
			System.out.println("percentage of blocked packets: " + twoDP.format((totalBlockedPackets/totalPackets)*100)+"\n");
			System.out.println("average number of hops per circuit: " + twoDP.format(totalHops/totalCircuitRequest)+"\n");
			System.out.println("average cumulative propagation delay per circuit: " + twoDP.format(totalDelay/totalCircuitRequest)+"\n");


			sc.close();
			sc2.close();

		}
		catch (FileNotFoundException e) {}


	}

	private static void updateGraph(ArrayList<Character> path, int num){
		for(int i=0; i<path.size()-1; i++){
			
			Node from = graph.getNode(path.get(i), path.get(i+1));
			from.setLoad(from.getConnections()+num);
			
			Node to = graph.getNode(path.get(i+1),path.get(i));
			to.setLoad(to.getConnections()+num);

			graph.updateNode(from,to);
			graph.updateNode(to,from);
		}
	}
	
	private static int getPathDelay(ArrayList<Character> path){
		int totalDelay = 0;
		for(int i=0; i<path.size()-1; i++){
			Node n = graph.getNode(path.get(i), path.get(i+1));
			totalDelay += n.getDelay();
		}
		return totalDelay;
	}

	private static class ConnectionComparator implements Comparator<ConnectionState> {
		public int compare(ConnectionState c1, ConnectionState c2) {
			if ((c1.getTime() > c2.getTime()) ) return 1;
			if ((c2.getTime() > c1.getTime()) ) return -1;
			return 0;
		}
	}


	private static AdjacencyListGraph graph;
	private static ArrayList<Character> path;
	private static Scanner sc;
	private static Scanner sc2;
	public static final boolean START = true;
	public static final boolean STOP = false;
}
