import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;


public class BFS {
	
	private static boolean[] visited;
	private static boolean[] blocked;
	private static char[] parent;

	//reverse the input argument so the path will be going forward
	public static ArrayList<Character> findShortestPath(AdjacencyListGraph graph, char from, char to){
		//need a source and a destination variable to be passed in
		parent = new char[26];
		visited = new boolean[26];
		blocked = new boolean[26];

		ArrayList<Character> shortestPath = new ArrayList<Character>();

		//make priority queue
    	Queue<Edge> q = new LinkedList<Edge>();
    	char currPos = from;
		
		//initialise array
		for(int i=0; i<26; i++) {
			visited[i] = false;
			blocked[i] = false;
		}
		
		//source weight is 0
		parent[currPos-'A'] = currPos;
		visited[currPos-'A'] = true;

		Edge s = new Edge(currPos, currPos, 0);
		q.add(s);
		//get all the nodes that the source connects to
		while (!q.isEmpty() && currPos != to) {
			Edge currEdge = q.poll();
			currPos = currEdge.getDestination();
			
			ArrayList<Node> connectedFrom = graph.getConnectedNodes(currEdge.getDestination());

			//loop through each node that are connected
			for(Node n : connectedFrom) {
				
				//add to queue, the node and total weight
				if (!visited[n.getName()-'A']) {
					//if current total weight is less than existing total weight, replace it
					// and also replace the parent, put the edge in queue
					parent[n.getName()-'A']= currEdge.getDestination();
					if(n.getLoad() == 1){
						blocked[n.getName()-'A'] = true;
					}
					
					//need to make sure that this puts at the end
					q.add(new Edge(currEdge.getDestination(), n.getName(), 1));
				}
				//the node is now visited

				visited[n.getName()-'A'] = true;
			}
			
		}
		//backtracks from the parent
		currPos = to;
		
		while (currPos != from) {
		    shortestPath.add(currPos);
		    if(currPos == 0 || blocked[currPos-'A'] == true){

				shortestPath.clear();
				return shortestPath;
			}
		    currPos = parent[currPos-'A'];
		}
	    shortestPath.add(currPos);
	    Collections.reverse(shortestPath);
		return shortestPath;
	}
}
