

public class Edge {
	public Edge(char src, char dest, double w){
		source = src;
		destination = dest;
		weight = w;
	}
	
	public char getSource(){
		return source;
	}
	
	public char getDestination(){
		return destination;
	}
	
	public double getWeight(){
		return weight;
	}
	
	private char source;
	private char destination;
	private double weight;
}
