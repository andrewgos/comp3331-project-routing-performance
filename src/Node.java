
public class Node {
	public Node(char id, int v1, int v2){
		name = id;
		propDelay = v1;
		maxConnections = v2;
		load = 0;
		connections = 0;
	}
	
	public char getName(){
		return name;
	}
	
	public int getDelay(){
		return propDelay;
	}
	
	public void setLoad(int current) {
		connections = current;
		load = connections/(double)maxConnections;
	}
	
	public double getLoad(){
		return load;
	}
	
	public int getCapacity(){
		return maxConnections;
	}
	
	public int getConnections(){
		return connections;
	}
	
	private char name;
	private int propDelay;
	private int maxConnections;
	private int connections;
	private double load;
}
